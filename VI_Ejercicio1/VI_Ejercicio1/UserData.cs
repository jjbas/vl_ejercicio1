﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace VI_Ejercicio1
{
    class UserData
    {
        static string ConvertStringArrayToString(string[] array)
        {
            // Concatenate all the elements into a StringBuilder.
            StringBuilder builder = new StringBuilder();
            foreach (string value in array)
            {
                builder.Append(value);
                builder.Append(Environment.NewLine);
            }
            return builder.ToString();
        }

        public static string GetUserData(string UserName)
        {
            try
            {
                //Obtenemos los datos del fichero en un string
                string[] userNameData = System.IO.File.ReadAllLines(@".\Datos\" + UserName + ".txt");
                string[] lines = userNameData.Skip(1).ToArray();
                return ConvertStringArrayToString(lines);
            }
            catch (Exception ex)
            {
                return "GetUserData: Error al abrir el fichero y obtener los datos. Exception: " + ex.Message.ToString();
            }
        }


        public static void SaveUserData(string UserName, string UserData)
        {
            //Obtenemos la primera linea del fichero
            var logFile = File.ReadAllLines(@".\Datos\" + UserName + ".txt");
            string firstLine = logFile[0]+ Environment.NewLine;

            //Borramos el fichero
            System.IO.File.Delete(@".\Datos\" + UserName + ".txt");
            
            //Guardamos la primera línea
            System.IO.File.WriteAllText(@".\Datos\" + UserName + ".txt", firstLine);

            //Añadimos el nuevo texto
            System.IO.File.AppendAllText(@".\Datos\" + UserName + ".txt", UserData);
        }

        public static void NewUser(string UserName, string FullName, string Age, string Sex)
        {
            if (UserName != "Users")
            { 
                //Borramos el fichero del usuario
                System.IO.File.Delete(@".\Datos\" + UserName + ".txt");
                //Lo volvemos a crear con el nuevo texto
                System.IO.File.WriteAllText(@".\Datos\" + UserName + ".txt", FullName+";"+Age+";"+Sex+";");


                //Obtenemos todos los usuarios
                var logFile = File.ReadAllLines(@".\Datos\" + "Users.txt");

                //Borramos el fichero
                System.IO.File.Delete(@".\Datos\" + "Users.txt");
                //Añadimos el nuevo usuario en el fichero de usuarios
                System.IO.File.WriteAllText(@".\Datos\" + "Users.txt", UserName+ Environment.NewLine);

                foreach (var s in logFile)
                {
                    //Lo volvemos a crear con el nuevo texto
                    System.IO.File.AppendAllText(@".\Datos\" + "Users.txt", s + Environment.NewLine);
                }
              
            }
        }

        public static void DeleteUser(string UserName)
        {
            if (UserName != "Users")
            {
                //Borramos el fichero del usuario
                System.IO.File.Delete(@".\Datos\" + UserName + ".txt");

                //Obtenemos todos los usuarios
                var logFile = File.ReadAllLines(@".\Datos\" + "Users.txt");
                
                //Borramos el fichero de usuarios
                System.IO.File.Delete(@".\Datos\" + "Users.txt");

                foreach (var s in logFile)
                {
                    if (s!= UserName)
                    { 
                        //Lo volvemos a crear con el nuevo texto
                        System.IO.File.AppendAllText(@".\Datos\" + "Users.txt", s + Environment.NewLine);
                    }
                }
            }
        }
        
        public static string[] GetUserNames()
        {
            List<String> listaUsuarios = new List<string>();
            try
            { 
                var logFile = File.ReadAllLines(@".\Datos\" + "Users.txt");
                foreach (var s in logFile) listaUsuarios.Add(s);
                return listaUsuarios.ToArray(); 
            }
            catch
            {
                return listaUsuarios.ToArray();
            }
        }

        public static string GetUserAge(string UserName)
        {
            try
            {
                //Obtenemos los datos del fichero en un string
                string userData = System.IO.File.ReadAllText(@".\Datos\" + UserName + ".txt");
                var age = userData.Split(new[] { Environment.NewLine }, StringSplitOptions.None).ToList().First();
                return age;
            }
            catch (Exception ex)
            {
                return "GetUserAge: Error al abrir el fichero y obtener los datos. Exception: " + ex.Message.ToString();
            }
        }
    }
}