﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VI_Ejercicio1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Llamar a la clase y rellenar el txt
            textBox1.Text= UserData.GetUserData(comboBox1.SelectedItem.ToString());
            cmdEdit.Visible = cmdSave.Visible = true;
            cmdSave.Enabled = false;
            cmdEdit.Enabled = true;
            textBox1.ReadOnly = true;
            try
            {
                lblUserN.Text = "Nombre Completo: " + UserData.GetUserAge(comboBox1.SelectedItem.ToString()).Split(';')[0];
                lblEdad.Text = "Edad: " + UserData.GetUserAge(comboBox1.SelectedItem.ToString()).Split(';')[1];
                lblSex.Text = "Sexo: " + UserData.GetUserAge(comboBox1.SelectedItem.ToString()).Split(';')[2];
            }
            catch
            {
            }
        }

        private void cmdEdit_Click(object sender, EventArgs e)
        {
            textBox1.ReadOnly = false;
            cmdSave.Enabled = true;
            cmdEdit.Enabled = false;
        }

        private void cmdSave_Click(object sender, EventArgs e)
        {
            try
            {
                //Llamar a la clase para guardar el txt
                UserData.SaveUserData(comboBox1.SelectedItem.ToString(), textBox1.Text);
                MessageBox.Show("Guardado");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            comboBox1.Items.Clear();
            comboBox1.Items.AddRange(UserData.GetUserNames());
        }

        private void cmdCrear_Click(object sender, EventArgs e)
        {
            if (txtUser.Text == "")
            {
                MessageBox.Show("El usuario no puede estar vacío");
                return;
            }
            if (comboBox1.Items.Contains(txtUser.Text))
            {
                MessageBox.Show("El usuario ya existe");
                txtUser.Text = "";
                return;
            }
            comboBox1.Items.Add(txtUser.Text);
            //comboBox1.SelectedIndex = comboBox1.Items.Count - 1;
            UserData.NewUser(txtUser.Text, txtFullName.Text, nudAge.Value.ToString(), txtSex.Text);
            //cmdEdit_Click(this, EventArgs.Empty);
            
        }

        private void cmdDelete_Click(object sender, EventArgs e)
        {
            UserData.DeleteUser(comboBox1.SelectedItem.ToString());
            comboBox1.Items.Clear();
            comboBox1.Items.AddRange(UserData.GetUserNames());
            lblSex.Text=lblUserN.Text= lblEdad.Text = "";
        }
    }
}
